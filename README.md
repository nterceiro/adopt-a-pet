# Docker commands

```
docker run --name sonarqube -p 9000:9000 -d sonarqube
docker run --name mongo -p 27017:27017 -d mongo
docker exec -it mongo bash
```

# Mongo commands

* Start mongo
```
mongo
```
* Show all databases
```
show dbs
```
* See sample queries in test > resources > sql file
* More info in https://docs.mongodb.com/manual/tutorial/getting-started/

# Run AdoptAPetApplication with VM parameter

`-Dspring.config.location=src/test/resources/test.properties`
