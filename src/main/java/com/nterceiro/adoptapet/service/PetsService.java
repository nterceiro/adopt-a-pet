package com.nterceiro.adoptapet.service;

import com.nterceiro.adoptapet.dao.PetsRepository;
import com.nterceiro.adoptapet.entity.MultiLangField;
import com.nterceiro.adoptapet.entity.PetEntity;
import com.nterceiro.adoptapet.exception.PetException;
import com.nterceiro.adoptapet.model.FilterRequest;
import com.nterceiro.adoptapet.model.FullPet;
import com.nterceiro.adoptapet.model.Pet;
import com.nterceiro.adoptapet.model.PetsPage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PetsService {

    private static final String NOT_FOUND_LANGUAGE_ERROR_MESSAGE = "Language %s is not available.";

    private PetsRepository petsRepository;
    private ModelMapper modelMapper;

    public PetsService(@Autowired PetsRepository petsRepository, @Autowired ModelMapper modelMapper) {
        this.petsRepository = petsRepository;
        this.modelMapper = modelMapper;
    }

    public PetsPage findBySpecies(FilterRequest filter) {
        Pageable pageable = PageRequest.of(filter.getCurrentPage() - 1, filter.getMaxPetsPerPage());
        Page<PetEntity> petEntities = filter.getFilter() == null || filter.getFilter().isEmpty() ?
                petsRepository.findAll(pageable) : petsRepository.findBySpecies(filter.getFilter(), pageable);
        return buildPetsPage(petEntities);
    }

    public FullPet findById(String petId, String lang) throws PetException {
        Optional<PetEntity> pet = petsRepository.findByUuid(petId);
        if (pet.isPresent()) {
            MultiLangField story = pet.get().getStory().stream()
                    .filter(st -> st.getLang().equalsIgnoreCase(lang))
                    .findFirst()
                    .orElse(null);
            MultiLangField subspecies = pet.get().getSubspecies() == null ? null :
                    pet.get().getSubspecies().stream()
                            .filter(sb -> sb.getLang().equalsIgnoreCase(lang))
                            .findFirst()
                            .orElseThrow(() -> new PetException(String.format(NOT_FOUND_LANGUAGE_ERROR_MESSAGE, lang)));
            return buildPet(pet.get(), story, subspecies);
        } else
            return null;
    }

    private PetsPage buildPetsPage(Page<PetEntity> petEntities) {
        return PetsPage.builder()
                .currentPage(petEntities.getNumber() + 1)
                .totalPages(petEntities.getTotalPages())
                .pets(petEntities.stream().map(pet -> modelMapper.map(pet, Pet.class)).collect(Collectors.toList()))
                .build();
    }

    private FullPet buildPet(PetEntity pet, MultiLangField story, MultiLangField subspecies) {
        return FullPet.builder()
                .name(pet.getName())
                .gender(pet.getGender())
                .species(pet.getSpecies())
                .subspecies(subspecies == null ? null : subspecies.getValue())
                .size(pet.getSize())
                .age(pet.getAge())
                .place(pet.getPlace())
                .story(story.getValue())
                .imageUrl(pet.getImageUrl())
                .isAdopted(pet.isAdopted())
                .build();
    }

}
