package com.nterceiro.adoptapet.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class MultiLangField {

    @NonNull
    private String lang;
    @NonNull
    private String value;

}
