package com.nterceiro.adoptapet.entity;

import com.nterceiro.adoptapet.model.Age;
import com.nterceiro.adoptapet.model.Gender;
import com.nterceiro.adoptapet.model.Size;
import com.nterceiro.adoptapet.model.Species;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;
import java.util.List;

@Data
@NoArgsConstructor
@Document(collection = "pets")
public class PetEntity {

    @Id
    private BigInteger id;
    @NonNull
    private String uuid;
    @NonNull
    private String name;
    @NonNull
    private Gender gender;
    @NonNull
    private Species species;
    private List<MultiLangField> subspecies;
    private Size size;
    private Age age;
    private String place;
    @NonNull
    private List<MultiLangField> story;
    @NonNull
    private String imageUrl;
    @NonNull
    private boolean isAdopted;

}
