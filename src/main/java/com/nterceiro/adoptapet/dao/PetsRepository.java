package com.nterceiro.adoptapet.dao;

import com.nterceiro.adoptapet.entity.PetEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface PetsRepository extends MongoRepository<PetEntity, BigInteger> {

    @Query(value = "{ 'species' : {$in: ?0} }")
    Page<PetEntity> findBySpecies(List<String> filter, Pageable pageable);

    Optional<PetEntity> findByUuid(String uuid);

}
