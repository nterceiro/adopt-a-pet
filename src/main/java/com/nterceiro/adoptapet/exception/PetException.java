package com.nterceiro.adoptapet.exception;

public class PetException extends Exception {

    public PetException(String msg) {
        super(msg);
    }

}
