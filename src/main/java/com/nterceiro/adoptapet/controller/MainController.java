package com.nterceiro.adoptapet.controller;

import com.nterceiro.adoptapet.dao.PetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class MainController {

    @Autowired
    PetsRepository repository;

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/{template}")
    public String anyTemplate(@PathVariable("template") String template) {
        final String folder = "fragments/";
        return template.indexOf(".html") == -1 ? folder + template
                : folder + template.substring(0, template.indexOf(".html"));
    }

}
