package com.nterceiro.adoptapet.controller;

import com.nterceiro.adoptapet.exception.PetException;
import com.nterceiro.adoptapet.model.FilterRequest;
import com.nterceiro.adoptapet.model.FullPet;
import com.nterceiro.adoptapet.model.PetsPage;
import com.nterceiro.adoptapet.service.PetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;

@RestController
@RequestMapping("/rest")
public class PetsRestController {

    private static final String PETS_PARAMETERS_EXCEPTION =
            "Parameters maxPetsPerPage and currentPage must be more than zero";

    @Value("${adoptapet.config.default.petsperpage}")
    private int petsPerPage;
    @Value("${adoptapet.config.default.currentpage}")
    private int currentPage;

    @Autowired
    private PetsService service;

    @PostMapping(value = "/pets", consumes = MediaType.APPLICATION_JSON_VALUE)
    public PetsPage filteredPets(@RequestBody(required = false) FilterRequest filter) {
        if (filter == null) {
            filter = buildDefaultFilterRequest();
        } else {
            if (filter.getMaxPetsPerPage() == null)
                filter.setMaxPetsPerPage(8);
            if (filter.getCurrentPage() == null)
                filter.setCurrentPage(1);
        }

        if (filter.getMaxPetsPerPage() <= 0 || filter.getCurrentPage() <= 0)
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, PETS_PARAMETERS_EXCEPTION);
        return service.findBySpecies(filter);
    }

    @GetMapping("/pet")
    public FullPet findById(@RequestParam(required = true) String petId,
                            @RequestParam(required = true) String lang) {
        try {
            return service.findById(petId, lang);
        } catch (PetException e) {
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, e.getMessage());
        }
    }

    private FilterRequest buildDefaultFilterRequest() {
        FilterRequest filterRequest = new FilterRequest();
        filterRequest.setMaxPetsPerPage(Integer.valueOf(petsPerPage));
        filterRequest.setCurrentPage(Integer.valueOf(currentPage));
        filterRequest.setFilter(Collections.emptyList());
        return filterRequest;
    }

}
