package com.nterceiro.adoptapet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdoptAPetApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdoptAPetApplication.class, args);
	}

}
