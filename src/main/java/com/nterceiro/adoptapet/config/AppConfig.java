package com.nterceiro.adoptapet.config;

import com.nterceiro.adoptapet.entity.PetEntity;
import com.nterceiro.adoptapet.model.Pet;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.typeMap(Pet.class, PetEntity.class)
                .addMapping(Pet::getId, PetEntity::setUuid);
        modelMapper.typeMap(PetEntity.class, Pet.class)
                .addMapping(PetEntity::getUuid, Pet::setId);

        return modelMapper;
    }

}
