package com.nterceiro.adoptapet.model;

public enum Species {
    CAT, DOG, BIRD, FISH, HORSE, RODENT, REPTILE
}
