package com.nterceiro.adoptapet.model;

public enum Size {
    GIANT, BIG, MEDIUM, SMALL
}
