package com.nterceiro.adoptapet.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Pet {

    private String id;
    private String name;
    private Gender gender;
    private Species species;
    private String place;
    private String imageUrl;
    private boolean isAdopted;

}
