package com.nterceiro.adoptapet.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FullPet {

    private String name;
    private Gender gender;
    private Species species;
    private String subspecies;
    private Size size;
    private Age age;
    private String place;
    private String story;
    private String imageUrl;
    private boolean isAdopted;

}
