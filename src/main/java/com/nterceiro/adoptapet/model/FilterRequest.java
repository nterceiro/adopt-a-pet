package com.nterceiro.adoptapet.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class FilterRequest {

    private Integer maxPetsPerPage;
    private Integer currentPage;
    private List<String> filter;

}
