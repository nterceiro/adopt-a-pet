package com.nterceiro.adoptapet.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Age {

    @NonNull
    private int value;
    @NonNull
    private ChronoUnit timeUnit;

}
