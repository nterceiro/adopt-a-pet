package com.nterceiro.adoptapet.model;

public enum Gender {
    MALE, FEMALE
}
