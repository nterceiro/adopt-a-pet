app.config(function ($translateProvider) {
    $translateProvider
    .useStaticFilesLoader({
        prefix: 'js/locales/locale-',
        suffix: '.json'
    })
    .useSanitizeValueStrategy('sanitizeParameters')
    .preferredLanguage('es');
});
