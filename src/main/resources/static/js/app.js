var app = angular.module('adoptAPetApp', ['pascalprecht.translate']);

app.run(['$rootScope', function($rootScope) {
    $rootScope.lang = 'es';
}])
