app.controller('mainController', function($scope, $rootScope, $translate, $http) {
    $scope.loading = true;
    $http.post('/rest/pets', null).then(function(response) {
        $scope.loading = false;
        $scope.pets = response.data.pets;
        $scope.petPerPage = 8;
        $scope.totalPages = response.data.totalPages;
        reloadPaginator();
        $scope.active = response.data.currentPage;

        function reloadPaginator() {
            $scope.pages = [];
            for (var i = 0; i < $scope.totalPages; i++) {
                $scope.pages.push(i+1);
            }
        }

        $scope.availableSpecies = ["CAT","DOG","BIRD","FISH","HORSE","RODENT","REPTILE"];
        $scope.checkedSpecies = [];
        $scope.toggleCheck = function (fruit) {
            if ($scope.checkedSpecies.indexOf(fruit) === -1) {
                $scope.checkedSpecies.push(fruit);
            } else {
                $scope.checkedSpecies.splice($scope.checkedSpecies.indexOf(fruit), 1);
            }
            $scope.findPage($scope.active);
        };

        $scope.genderIcon = function(input) {
            return input === "MALE" ? "fa-mars" : "fa-venus";
        }
        $scope.speciesIcon = function(input) {
            var result;
            switch(input) {
                case "CAT":
                    result = "fa-cat";
                    break;
                case "DOG":
                    result = "fa-dog";
                    break;
                case "BIRD":
                    result = "fa-dove";
                    break;
                case "FISH":
                    result = "fa-fish";
                    break;
                case "HORSE":
                    result = "fa-horse-head";
                    break;
                case "RODENT":
                    result = "fa-otter";
                    break;
                case "REPTILE":
                    result = "fa-frog";
                    break;
                default:
                    result = "fa-paw";
                    break;
            }
            return result;
        }
        
        $scope.showPet = function(id) {
            $scope.loadingPet = true;
            $http.get('/rest/pet?petId=' + id + "&lang=" + $rootScope.lang).then(function(response) {
                $scope.loadingPet = false;
                $scope.pet = response.data;
            });
        }
        
        $scope.changePetsPerPage = function(petsPerPage) {
            $scope.petPerPage = petsPerPage;
            $scope.findPage(1);
        }
        $scope.findPage = function(page) {
            var postData = {
                maxPetsPerPage: $scope.petPerPage,
                currentPage: page,
                filter: $scope.checkedSpecies
            };
            $scope.loading = true;
            $http.post('/rest/pets', JSON.stringify(postData)).then(function(response) {
                $scope.loading = false;
                $scope.pets = response.data.pets;
                $scope.totalPages = response.data.totalPages;
                reloadPaginator();
                $scope.active = response.data.currentPage;
            });
        }
    });

    $scope.changeLanguage = function (key) {
        $rootScope.lang = key;
        $translate.use(key);
    };
});