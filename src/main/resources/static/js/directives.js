app.directive("petModal", function() {
    return {
        templateUrl: "pet-modal.html"
    }
}).directive("languageMenu", function() {
    return {
        templateUrl: "language-menu.html"
    }
}).directive("mainMenu", function() {
    return {
        templateUrl: "main-menu.html"
    }
})