package com.nterceiro.adoptapet.api;

import com.nterceiro.adoptapet.AdoptAPetApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdoptAPetApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableMongoRepositories("com.nterceiro.adoptapet.dao")
@TestPropertySource("classpath:tests.properties")
public class AdoptAPetApiTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testIndex() {
        ResponseEntity<String> response = restTemplate.getForEntity(buildBasicUrl(), String.class);

        assertTrue(response.getBody().contains("<html"));
        assertTrue(response.getBody().contains("<title>Adopt a pet</title>"));
    }

    @Test
    public void testTemplateWithExtension() {
        ResponseEntity<String> response = restTemplate.getForEntity(buildBasicUrl() + "pet-modal.html", String.class);

        assertTrue(response.getBody().contains("petModal"));
    }

    @Test
    public void testTemplateWithoutExtension() {
        ResponseEntity<String> response = restTemplate.getForEntity(buildBasicUrl() + "pet-modal", String.class);

        assertTrue(response.getBody().contains("petModal"));
    }

    private String buildBasicUrl() {
        return "http://localhost:" + port + "/";
    }
}
