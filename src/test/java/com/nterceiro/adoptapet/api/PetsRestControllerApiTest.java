package com.nterceiro.adoptapet.api;

import com.nterceiro.adoptapet.AdoptAPetApplication;
import com.nterceiro.adoptapet.dao.PetsRepository;
import com.nterceiro.adoptapet.entity.PetEntity;
import com.nterceiro.adoptapet.model.FilterRequest;
import com.nterceiro.adoptapet.model.FullPet;
import com.nterceiro.adoptapet.model.Pet;
import com.nterceiro.adoptapet.model.PetsPage;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.nterceiro.adoptapet.model.Species.HORSE;
import static com.nterceiro.adoptapet.utils.RepoUtils.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdoptAPetApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableMongoRepositories("com.nterceiro.adoptapet.dao")
@TestPropertySource("classpath:tests.properties")
public class PetsRestControllerApiTest {

    private final static int DEFAULT_PAGE_SIZE = 8;
    private final static String DOG_1_UUID = UUID.randomUUID().toString();
    private final static String DOG_2_UUID = UUID.randomUUID().toString();
    private final static String CAT_1_UUID = UUID.randomUUID().toString();
    private final static String CAT_2_UUID = UUID.randomUUID().toString();
    private final static String FISH_UUID = UUID.randomUUID().toString();
    private final static String BIRD_UUID = UUID.randomUUID().toString();
    private final static String HORSE_UUID = UUID.randomUUID().toString();
    private final static String RODENT_UUID = UUID.randomUUID().toString();
    private final static String REPTILE_UUID = UUID.randomUUID().toString();

    private static List<PetEntity> storedPets;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private PetsRepository petsRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${local.server.port}")
    private int port;

    @BeforeClass
    public static void buildPetList() {
        storedPets = Arrays.asList(buildDog1(DOG_1_UUID), buildCat1(CAT_1_UUID), buildFish(FISH_UUID),
                buildBird(BIRD_UUID), buildHorse(HORSE_UUID), buildRodent(RODENT_UUID),
                buildReptile(REPTILE_UUID), buildDog2(DOG_2_UUID), buildCat2(CAT_2_UUID));
    }

    @Before
    public void setUp() {
        petsRepo.saveAll(storedPets);
    }

    @After
    public void tearDown() {
        petsRepo.deleteAll();
    }

    @Test
    public void testFilteredPets() {
        String url = "http://localhost:" + port + "/rest/pets";
        int petsPerPage = 2;
        int currentPage = 1;
        HttpEntity<FilterRequest> entity = new HttpEntity<>(buildFilterRequest(petsPerPage, currentPage, Arrays.asList(HORSE.toString())));
        ResponseEntity<PetsPage> response = restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(currentPage, response.getBody().getCurrentPage());
        assertEquals(1, response.getBody().getTotalPages());
        assertEquals(1, response.getBody().getPets().size());
        assertTrue(isInList(response.getBody().getPets()));
    }

    @Test
    public void testNoFilteredPets() {
        String url = "http://localhost:" + port + "/rest/pets";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<FilterRequest> entity = new HttpEntity<>(headers);
        ResponseEntity<PetsPage> response = restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);
        int totalPages = (int) Math.ceil(Double.valueOf(storedPets.size()) / Double.valueOf(DEFAULT_PAGE_SIZE));

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getCurrentPage());
        assertEquals(totalPages, response.getBody().getTotalPages());
        assertEquals(8, response.getBody().getPets().size());
        assertTrue(isInList(response.getBody().getPets()));
    }

    @Test
    public void testEmptyFilteredPets() {
        String url = "http://localhost:" + port + "/rest/pets";
        HttpEntity<FilterRequest> entity = new HttpEntity<>(new FilterRequest());
        ResponseEntity<PetsPage> response = restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);
        int totalPages = (int) Math.ceil(Double.valueOf(storedPets.size()) / Double.valueOf(DEFAULT_PAGE_SIZE));

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getCurrentPage());
        assertEquals(totalPages, response.getBody().getTotalPages());
        assertEquals(8, response.getBody().getPets().size());
        assertTrue(isInList(response.getBody().getPets()));
    }

    @Test
    public void testWrongFilteredPets() {
        String url = "http://localhost:" + port + "/rest/pets";
        HttpEntity<FilterRequest> entity = new HttpEntity<>(buildFilterRequest(DEFAULT_PAGE_SIZE, 1, Arrays.asList("RANDOM")));
        ResponseEntity<PetsPage> response = restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().getPets().isEmpty());
        assertEquals(1, response.getBody().getCurrentPage());
        assertEquals(0, response.getBody().getTotalPages());
    }

    @Test(expected = HttpClientErrorException.class)
    public void testFilteredPetsWithWrongMaxPetsParams() {
        String url = "http://localhost:" + port + "/rest/pets";
        HttpEntity<FilterRequest> entity = new HttpEntity<>(buildFilterRequest(-1, 1, Arrays.asList(HORSE.toString())));
        restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);
    }

    @Test(expected = HttpClientErrorException.class)
    public void testFilteredPetsWithWrongCurrentPageParams() {
        String url = "http://localhost:" + port + "/rest/pets";
        HttpEntity<FilterRequest> entity = new HttpEntity<>(buildFilterRequest(DEFAULT_PAGE_SIZE, 0, Arrays.asList(HORSE.toString())));
        restTemplate.exchange(url, HttpMethod.POST, entity, PetsPage.class);
    }

    @Test
    public void testFindById() {
        String petId = String.valueOf(storedPets.get(0).getId());
        String url = "http://localhost:" + port + "/rest/pet?petId=" + DOG_1_UUID + "&lang=ES";
        ResponseEntity<FullPet> response = restTemplate.getForEntity(url, FullPet.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(buildExpectedPet(buildDog1(DOG_1_UUID)), response.getBody());
    }

    @Test
    public void testFindByIdNotFound() {
        String url = "http://localhost:" + port + "/rest/pet?petId=0&lang=ES";
        ResponseEntity<FullPet> response = restTemplate.getForEntity(url, FullPet.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test(expected = HttpClientErrorException.class)
    public void testFindByIdWrongLang() {
        String url = "http://localhost:" + port + "/rest/pet?petId=" + DOG_2_UUID + "&lang=EN";
        restTemplate.getForEntity(url, FullPet.class);
    }

    private boolean isInList(List<Pet> pets) {
        List<Pet> mappedPets = storedPets.stream()
                .map(pet -> modelMapper.map(pet, Pet.class))
                .collect(Collectors.toList());
        return pets.stream().allMatch(pet -> mappedPets.contains(pet));
    }

}
