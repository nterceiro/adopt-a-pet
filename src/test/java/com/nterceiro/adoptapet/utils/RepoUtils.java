package com.nterceiro.adoptapet.utils;

import com.nterceiro.adoptapet.entity.MultiLangField;
import com.nterceiro.adoptapet.entity.PetEntity;
import com.nterceiro.adoptapet.model.Age;
import com.nterceiro.adoptapet.model.FilterRequest;
import com.nterceiro.adoptapet.model.FullPet;

import java.util.Arrays;
import java.util.List;

import static com.nterceiro.adoptapet.model.Gender.FEMALE;
import static com.nterceiro.adoptapet.model.Gender.MALE;
import static com.nterceiro.adoptapet.model.Size.*;
import static com.nterceiro.adoptapet.model.Species.*;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.YEARS;

public class RepoUtils {

    public static final String DEFAULT_DESCRIPTION = "Lorem ipsum dolor sit amet.";
    public static final String DEFAULT_SUBSPECIES = "Subspecies";

    public static PetEntity buildDog1(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Firulais");
        pet.setGender(MALE);
        pet.setSpecies(DOG);
        pet.setSubspecies(Arrays.asList(buildSubspecies("ES")));
        pet.setSize(BIG);
        pet.setAge(new Age(6, YEARS));
        pet.setPlace("Madrid");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(false);
        return pet;
    }

    public static PetEntity buildCat1(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Lucifer");
        pet.setGender(MALE);
        pet.setSpecies(CAT);
        pet.setSubspecies(Arrays.asList(buildSubspecies("ES")));
        pet.setAge(new Age(8, YEARS));
        pet.setPlace("Bilbao");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(true);
        return pet;
    }

    public static PetEntity buildFish(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Dory");
        pet.setGender(FEMALE);
        pet.setSpecies(FISH);
        pet.setPlace("Valencia");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(false);
        return pet;
    }

    public static PetEntity buildBird(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Piolín");
        pet.setGender(MALE);
        pet.setSpecies(BIRD);
        pet.setSize(MEDIUM);
        pet.setAge(new Age(11, MONTHS));
        pet.setPlace("Madrid");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(true);
        return pet;
    }

    public static PetEntity buildHorse(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Maximus");
        pet.setGender(MALE);
        pet.setSpecies(HORSE);
        pet.setAge(new Age(12, YEARS));
        pet.setPlace("Zaragoza");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(false);
        return pet;
    }

    public static PetEntity buildRodent(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Minnie");
        pet.setGender(FEMALE);
        pet.setSpecies(RODENT);
        pet.setAge(new Age(9, YEARS));
        pet.setPlace("Tenerife");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(true);
        return pet;
    }

    public static PetEntity buildReptile(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Pascal");
        pet.setGender(MALE);
        pet.setSpecies(REPTILE);
        pet.setSize(SMALL);
        pet.setAge(new Age(3, YEARS));
        pet.setPlace("Ávila");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(false);
        return pet;
    }

    public static PetEntity buildDog2(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Scooby Doo");
        pet.setGender(MALE);
        pet.setSpecies(DOG);
        pet.setSubspecies(Arrays.asList(buildSubspecies("ES")));
        pet.setSize(GIANT);
        pet.setAge(new Age(4, YEARS));
        pet.setPlace("Madrid");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(false);
        return pet;
    }

    public static PetEntity buildCat2(String uuid) {
        PetEntity pet = new PetEntity();
        pet.setUuid(uuid);
        pet.setName("Duquesa");
        pet.setGender(FEMALE);
        pet.setSpecies(CAT);
        pet.setSubspecies(Arrays.asList(buildSubspecies("ES")));
        pet.setAge(new Age(5, YEARS));
        pet.setPlace("Barcelona");
        pet.setStory(Arrays.asList(buildStory("ES")));
        pet.setImageUrl("URL");
        pet.setAdopted(true);
        return pet;
    }

    public static MultiLangField buildStory(String lang) {
        MultiLangField story = new MultiLangField();
        story.setLang(lang);
        story.setValue(lang + ": " + DEFAULT_DESCRIPTION);
        return story;
    }

    public static MultiLangField buildSubspecies(String lang) {
        MultiLangField subspecies = new MultiLangField();
        subspecies.setLang(lang);
        subspecies.setValue(lang + ": " + DEFAULT_SUBSPECIES);
        return subspecies;
    }

    public static FilterRequest buildFilterRequest(int maxPetsPerPage, int currentPage, List<String> filter) {
        FilterRequest filterRequest = new FilterRequest();
        filterRequest.setMaxPetsPerPage(maxPetsPerPage);
        filterRequest.setCurrentPage(currentPage);
        filterRequest.setFilter(filter);
        return filterRequest;
    }

    public static FullPet buildExpectedPet(PetEntity pet) {
        return FullPet.builder()
                .name(pet.getName())
                .gender(pet.getGender())
                .species(pet.getSpecies())
                .subspecies(pet.getSubspecies() == null ? null : pet.getSubspecies().get(0).getValue())
                .size(pet.getSize())
                .age(pet.getAge())
                .place(pet.getPlace())
                .story(pet.getStory().get(0).getValue())
                .imageUrl(pet.getImageUrl())
                .isAdopted(pet.isAdopted())
                .build();
    }

}
