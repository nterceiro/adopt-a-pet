package com.nterceiro.adoptapet.integration;

import com.nterceiro.adoptapet.AdoptAPetApplication;
import com.nterceiro.adoptapet.dao.PetsRepository;
import com.nterceiro.adoptapet.entity.PetEntity;
import com.nterceiro.adoptapet.exception.PetException;
import com.nterceiro.adoptapet.model.FullPet;
import com.nterceiro.adoptapet.model.Pet;
import com.nterceiro.adoptapet.model.PetsPage;
import com.nterceiro.adoptapet.service.PetsService;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.nterceiro.adoptapet.model.Species.CAT;
import static com.nterceiro.adoptapet.model.Species.DOG;
import static com.nterceiro.adoptapet.utils.RepoUtils.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdoptAPetApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableMongoRepositories("com.nterceiro.adoptapet.dao")
@TestPropertySource("classpath:tests.properties")
public class PetsServiceIntegrationTest {

    private final static String DOG_1_UUID = UUID.randomUUID().toString();
    private final static String DOG_2_UUID = UUID.randomUUID().toString();
    private final static String CAT_1_UUID = UUID.randomUUID().toString();
    private final static String CAT_2_UUID = UUID.randomUUID().toString();
    private final static String FISH_UUID = UUID.randomUUID().toString();
    private final static String BIRD_UUID = UUID.randomUUID().toString();
    private final static String HORSE_UUID = UUID.randomUUID().toString();
    private final static String RODENT_UUID = UUID.randomUUID().toString();
    private final static String REPTILE_UUID = UUID.randomUUID().toString();

    private static List<PetEntity> storedPets;

    @Autowired
    private PetsRepository petsRepo;

    @Autowired
    private PetsService service;

    @Autowired
    private ModelMapper modelMapper;

    @BeforeClass
    public static void buildPetList() {
        storedPets = Arrays.asList(buildDog1(DOG_1_UUID), buildCat1(CAT_1_UUID), buildFish(FISH_UUID),
                buildBird(BIRD_UUID), buildHorse(HORSE_UUID), buildRodent(RODENT_UUID),
                buildReptile(REPTILE_UUID), buildDog2(DOG_2_UUID), buildCat2(CAT_2_UUID));
    }

    @Before
    public void setUp() {
        petsRepo.saveAll(storedPets);
    }

    @After
    public void tearDown() {
        petsRepo.deleteAll();
    }

    @Test
    public void testFindBySpecies() {
        List<String> filter = Arrays.asList(DOG.toString());
        PetsPage dogs = service.findBySpecies(buildFilterRequest(8, 1, filter));

        assertEquals(2, dogs.getPets().size());
        assertEquals(1, dogs.getCurrentPage());
        assertEquals(1, dogs.getTotalPages());

        assertTrue(dogs.getPets().contains(modelMapper.map(buildDog1(DOG_1_UUID), Pet.class)));
        assertTrue(dogs.getPets().contains(modelMapper.map(buildDog2(DOG_2_UUID), Pet.class)));
    }

    @Test
    public void testFindBySpeciesMultiple() {
        List<String> filter = Arrays.asList(DOG.toString(), CAT.toString());
        PetsPage catsAndDogsPage1 = service.findBySpecies(buildFilterRequest(3, 1, filter));
        PetsPage catsAndDogsPage2 = service.findBySpecies(buildFilterRequest(3, 2, filter));

        assertEquals(3, catsAndDogsPage1.getPets().size());
        assertEquals(1, catsAndDogsPage1.getCurrentPage());
        assertEquals(2, catsAndDogsPage1.getTotalPages());
        assertEquals(1, catsAndDogsPage2.getPets().size());
        assertEquals(2, catsAndDogsPage2.getCurrentPage());
        assertEquals(2, catsAndDogsPage2.getTotalPages());

        assertTrue(catsAndDogsPage1.getPets().contains(modelMapper.map(buildDog1(DOG_1_UUID), Pet.class))
                || catsAndDogsPage2.getPets().contains(modelMapper.map(buildDog1(DOG_1_UUID), Pet.class)));
        assertTrue(catsAndDogsPage1.getPets().contains(modelMapper.map(buildDog2(DOG_2_UUID), Pet.class))
                || catsAndDogsPage2.getPets().contains(modelMapper.map(buildDog2(DOG_2_UUID), Pet.class)));
        assertTrue(catsAndDogsPage1.getPets().contains(modelMapper.map(buildCat1(CAT_1_UUID), Pet.class))
                || catsAndDogsPage2.getPets().contains(modelMapper.map(buildCat1(CAT_1_UUID), Pet.class)));
        assertTrue(catsAndDogsPage1.getPets().contains(modelMapper.map(buildCat2(CAT_2_UUID), Pet.class))
                || catsAndDogsPage2.getPets().contains(modelMapper.map(buildCat2(CAT_2_UUID), Pet.class)));
    }

    @Test
    public void testFindBySpeciesEmptyFilter() {
        PetsPage allPets = service.findBySpecies(buildFilterRequest(10, 1, Collections.emptyList()));

        assertEquals(9, allPets.getPets().size());
        assertEquals(1, allPets.getCurrentPage());
        assertEquals(1, allPets.getTotalPages());

        assertTrue(allPets.getPets().contains(modelMapper.map(buildDog1(DOG_1_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildDog2(DOG_2_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildCat1(CAT_1_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildCat2(CAT_2_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildFish(FISH_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildBird(BIRD_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildHorse(HORSE_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildRodent(RODENT_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildReptile(REPTILE_UUID), Pet.class)));
    }

    @Test
    public void testFindBySpeciesNullFilter() {
        PetsPage allPets = service.findBySpecies(buildFilterRequest(10, 1, null));

        assertEquals(9, allPets.getPets().size());
        assertEquals(1, allPets.getCurrentPage());
        assertEquals(1, allPets.getTotalPages());

        assertTrue(allPets.getPets().contains(modelMapper.map(buildDog1(DOG_1_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildDog2(DOG_2_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildCat1(CAT_1_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildCat2(CAT_2_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildFish(FISH_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildBird(BIRD_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildHorse(HORSE_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildRodent(RODENT_UUID), Pet.class)));
        assertTrue(allPets.getPets().contains(modelMapper.map(buildReptile(REPTILE_UUID), Pet.class)));
    }

    @Test
    public void testFindById() throws PetException {
        String lang = "ES";
        FullPet dog1 = service.findById(DOG_1_UUID, lang);
        FullPet dog2 = service.findById(DOG_2_UUID, lang);
        FullPet cat1 = service.findById(CAT_1_UUID, lang);
        FullPet cat2 = service.findById(CAT_2_UUID, lang);
        FullPet fish = service.findById(FISH_UUID, lang);
        FullPet bird = service.findById(BIRD_UUID, lang);
        FullPet horse = service.findById(HORSE_UUID, lang);
        FullPet rodent = service.findById(RODENT_UUID, lang);
        FullPet reptile = service.findById(REPTILE_UUID, lang);

        assertEquals(buildExpectedPet(buildDog1(DOG_1_UUID)), dog1);
        assertEquals(buildExpectedPet(buildDog2(DOG_2_UUID)), dog2);
        assertEquals(buildExpectedPet(buildCat1(CAT_1_UUID)), cat1);
        assertEquals(buildExpectedPet(buildCat2(CAT_2_UUID)), cat2);
        assertEquals(buildExpectedPet(buildFish(FISH_UUID)), fish);
        assertEquals(buildExpectedPet(buildBird(BIRD_UUID)), bird);
        assertEquals(buildExpectedPet(buildHorse(HORSE_UUID)), horse);
        assertEquals(buildExpectedPet(buildRodent(RODENT_UUID)), rodent);
        assertEquals(buildExpectedPet(buildReptile(REPTILE_UUID)), reptile);
    }

    @Test(expected = PetException.class)
    public void testFindByIdSubspeciesLanguageException() throws PetException {
        service.findById(DOG_1_UUID, "EN");
    }

    @Test
    public void testFindByIdNotFound() throws PetException {
        FullPet pet = service.findById("invalid-id", "ES");

        assertNull(pet);
    }

}
